"""使用简单工厂方法， 实现timo 和 police 两个英雄
一个回合制游戏，有两个英雄，分别以两个类进行定义。分别是timo和police。每个英雄都有 hp 属性和 power属性，hp 代表血量，power 代表攻击力

每个英雄都有一个 fight 方法：
my_hp = hp - enemy_power
enemy_final_hp = enemy_hp - my_power
两个 hp 进行对比，血量剩余多的人获胜

每个英雄都一个speak_lines方法
调用speak_lines方法，不同的角色会打印（讲出）不同的台词
timo : 提莫队长正在待命
police: 见识一下法律的子弹
"""


# 定义英雄类
class Hero:
    name = 0
    power = 0
    hp = 0

    # 定义类方法 enemy_hp为敌人血量 enemy_power为敌人攻击力
    def fight(self, enemy_hp, enemy_power):
        self.speak_lines("timo")
        self.speak_lines("police")
        # 一回合结束自身剩余血量
        my_final_hp = self.hp - enemy_power
        # 一回合结束敌人剩余血量
        enemy_final_hp = enemy_hp - self.power
        # 如果self赢了，输出我赢了
        if my_final_hp >= enemy_final_hp:
            print(f"{self.name}赢了")
        # 如果敌人赢了，输出：敌人赢了
        elif my_final_hp <= enemy_final_hp:
            print("敌人赢了")
        # 如果打平手，则输出打平
        else:
            print("我们打平了")

    # 定义台词类方法
    def speak_lines(self, name):
        # 如果英雄名字为timo，则说timo的台词
        if name == 'timo':
            print("提莫队长正在待命")
        # 如果英雄名字为police，则说police的台词
        elif name == 'police':
            print("见识一下法律的子弹")
        # 如果都不是则抛出异常
        else:
            print("我没有台词")
            raise Exception("sorry")

